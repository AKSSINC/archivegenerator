using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using ArchiveGenerator.Models;

namespace ArchiveGenerator
{
	public class Program
	{
		public static int Errors = 0;
		public static int Warnings = 0;

		#region Enums (1)

		public enum DirectoryCommand
		{
			/// <summary> Пропустить </summary>
			Skip,
			/// <summary> Обработать </summary>
			Process,
			/// <summary> Уведомлять про каждый из дочерних каталогов, не имеющих конфиг </summary>
			EnsureSub
		}

		#endregion Enums

		#region Methods (8)

		// Public Methods (7) 

		public static string ProcessFolder(DirectoryInfo rootDirectory, int currentSubfolderLength, AppConfig appConfig, bool shouldContainConfig = false)
		{
			string result = "";
			FileInfo[] configFiles = rootDirectory.GetFiles("archive_config.xml");
			var processResult = new ProcessConfigResult { IsAllSubdirsMustContainConfigs = false };
			if (configFiles.Any())
			{
				processResult = ProcessConfigFile(rootDirectory, appConfig, configFiles[0]);
				result += processResult.OutputCommands;
				if (!processResult.ScanForChildren)
				{
					return result;
				}
			}
			else
			{
				//Конфигурационный файл в данной папке не обнаружен
				if (shouldContainConfig)
				{
					Warnings++;
					result += String.Format("@rem ADD {0}\n" , rootDirectory.FullName);
					Log(String.Format("Folder must contain config: \"{0}\"\n", rootDirectory.FullName), ConsoleColor.Yellow);
				}
			}
			if (currentSubfolderLength > AppConfig.MaxSubfolderLength)
			{
				return result;
			}
			foreach (DirectoryInfo dir in rootDirectory.GetDirectories())
			{
				result += ProcessFolder(dir, currentSubfolderLength + 1, appConfig, processResult.IsAllSubdirsMustContainConfigs);
			}
			return result;
		}

		public static void Log(string message, ConsoleColor color)
		{
			Debug.Write(message);
			Console.ForegroundColor = color;
			Console.Write(message);
			Console.ResetColor();
		}
		/// <summary> Генерирует команду архиватора для включения в реультирующий файл </summary>
		/// <param name="settings"></param>
		/// <param name="fileInfo"></param>
		/// <param name="appConfig"></param>
		/// <returns></returns>
		public static string GenerateOutputCommand(DirectorySettings settings, FileInfo fileInfo, AppConfig appConfig)
		{
			string directoryName = fileInfo.DirectoryName;
			if (settings.AutoFilename)
			{
				var newName = "Backup_"+fileInfo.Directory.Name.Replace(" ", "_");
				settings.Filename = newName;

			}
			Log(String.Format("Processing  ({0})\n", settings.Comment), ConsoleColor.Gray);
			var sb = new StringBuilder();
			if (settings.Comment != "")
			{
				sb.AppendFormat("@rem {0}\n", settings.Comment);
			}
			sb.AppendFormat("\"{0}\" a -p{1} -u -as -r -m{2} -cfg- -ep1{3}{4} \"{5}{6}.rar\" \"{7}\\*.*\" \n"
				, appConfig.WinRarPath
				, appConfig.Password
				, settings.Method //2
				,appConfig.RarCommandLine//3
				,settings.RarCommandLine//4
				, appConfig.BackupPath //5
				, settings.Filename//6
				, directoryName//7
				);
			return sb.ToString();
		}

		/// <summary> Ищет и обрабатываем конфигурационный файл в диретории </summary>
		/// <param name="rootDirectory">Директория для работы</param>
		/// <param name="appConfig">Конфигурация приложения</param>
		/// <param name="configFile"></param>
		/// <returns></returns>
		public static ProcessConfigResult ProcessConfigFile(DirectoryInfo rootDirectory, AppConfig appConfig, FileInfo configFile)
		{
			var result = new ProcessConfigResult { OutputCommands = "", ScanForChildren = true };

			string configFileName = configFile.FullName;
			Log(String.Format("Find AppConfig in {0}... ", configFileName), ConsoleColor.White);
			DirectorySettings settings = null;
			try
			{
				settings = Tools.LoadObjectFromFile<DirectorySettings>(configFileName);
				settings.LastError = null;
			}
			catch (XmlException ex)
			{
				Log(ex.Message + Environment.NewLine, ConsoleColor.DarkRed);
				Errors++;
				if (appConfig.ReCreateConfigsIfXmlErrorOnParse)
				{
					settings = new DirectorySettings { LastError = ex.Message, Filename = configFile.DirectoryName.Replace(Path.DirectorySeparatorChar, '_').Replace(Path.VolumeSeparatorChar, '_') };
					settings.Save(configFileName);
				}
				return new ProcessConfigResult { OutputCommands = "" };
			}

			if (settings.Command == DirectoryCommand.Skip)
			{
				Log(String.Format("*** {0} skipped. \n", rootDirectory), ConsoleColor.DarkGray);
				result.ScanForChildren = false;
			}
			if (settings.Command == DirectoryCommand.Process)
			{
				result.OutputCommands += GenerateOutputCommand(settings, configFile, appConfig);
				result.ScanForChildren = false;
			}
			if (settings.Command == DirectoryCommand.EnsureSub)
			{
				result.IsAllSubdirsMustContainConfigs = true;
				result.ScanForChildren = true;
			}
			settings.Save(configFileName);
			Log(result.OutputCommands + Environment.NewLine, ConsoleColor.DarkGreen);
			return result;
		}



		// Private Methods (1) 

		private static void Main(string[] args)
		{
			string configDirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string configFilePath = Path.Combine(configDirPath, "ArchiveGenerator.xml");


			AppConfig appConfig;
			try
			{
				appConfig = Tools.LoadObjectFromFile<AppConfig>(configFilePath);
			}
			catch (Exception ex)
			{
				Log(String.Format("Exception load AppConfig. Will create new: {0}\n", ex), ConsoleColor.DarkRed);
				Errors++;
				appConfig = new AppConfig();
			}
			Tools.SaveConfigToFile(appConfig, configFilePath);
			var rootDirectory = new DirectoryInfo(appConfig.SourcePath);
			string result = ProcessFolder(rootDirectory, 0, appConfig);
			string backupJodFilePath = appConfig.BackupPath + "DoBackupJob.bat";
			if (File.Exists(backupJodFilePath))
			{
				File.Delete(backupJodFilePath);
			}
			using (var stream = new FileStream(backupJodFilePath, FileMode.Create))
			using (var outfile = new StreamWriter(stream,Encoding.GetEncoding(866)))
			{
				outfile.WriteLine(result);
				outfile.WriteLine(string.Format("@rem ArchiveGenerator 2 at {0}",DateTime.Now.ToStringRus()));
				outfile.Close();
				stream.Close();
			}
			if (Errors > 0) { Log(String.Format("Errors: {0}",Errors),ConsoleColor.Red); }
			if (Warnings > 0) { Log(String.Format("Warnings: {0}", Warnings), ConsoleColor.Yellow); }
		}

		#endregion Methods

		#region Nested Classes (1)

		#endregion Nested Classes
	}
}