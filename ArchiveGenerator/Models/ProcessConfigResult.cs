namespace ArchiveGenerator.Models
{
	public class ProcessConfigResult
	{
		#region Fields (3)

		/// <summary> Нужно ли уведомлять про каждый из дочерних каталогов, не имеющих конфиг </summary>
		public bool IsAllSubdirsMustContainConfigs;

		public string OutputCommands;

		/// <summary> Нужно ли просматривать дочерние каталоги </summary>
		public bool ScanForChildren;

		#endregion Fields
	}
}