using System;
using System.Collections.Generic;
using System.Linq;

namespace ArchiveGenerator.Models
{
	/// <summary> Модель настроек конкретной директории </summary>
	public class DirectorySettings
	{
		#region Fields (5)
		/// <summary> Тип обработки директории </summary>
		public Program.DirectoryCommand Command = Program.DirectoryCommand.Skip;
		public string Filename = "filename";
		public string LastError;
		public string Comment="";
		public DateTime LastRun;
		public string XComment;
		public string Method = "5";
		public string RarCommandLine = "";

		/// <summary> Creates Filename auto as "Archive_" + folder name, spaces converted to underline </summary>
		public bool AutoFilename = false;

		#endregion Fields

		#region Methods (1)

		// Public Methods (1) 

		/// <summary>
		/// Сохраняет моджель по указанному пути
		/// </summary>
		/// <param name="filepath">Путь для сохранения конфигурации</param>
		public void Save(string filepath)
		{
			LastRun = DateTime.Now.ToLocalTime();
			XComment = String.Format("Commands are: {0}, {1} and {2}\n{3}\n{4}", Program.DirectoryCommand.Skip, Program.DirectoryCommand.Process,Program.DirectoryCommand.EnsureSub,
				"Methods are: 0-Strore ... 5-Max compression","To skip files use RarCommandLine: -x\".dropbox.cache\" -x\"Portable Documents\" -x\"PortableApps\" "
				);
			Tools.SaveConfigToFile(this, filepath);
		}

		#endregion Methods
	}
}
