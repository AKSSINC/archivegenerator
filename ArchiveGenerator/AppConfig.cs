using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ArchiveGenerator
{
	public class AppConfig
	{
		public static AppConfig LoadConfig(string filepath)
		{
			try
			{
				return Tools.LoadObjectFromFile<AppConfig>(Assembly.GetExecutingAssembly().Location);
			}
			catch (Exception ex)
			{
				Program.Log(String.Format("Exception load AppConfig. Will create new: {0}\n", ex), ConsoleColor.DarkRed);
			}
			return new AppConfig();
		}

		#region Fields (7)

		public static int MaxSubfolderLength = 2;
		//public string A = @" -u -as -r -m3 -cfg- -ep1 ""Install_Tools and utilites.rar"" ""C:\AK\Install\Windows Install\Tools and utilites\*.*""";
		public string BackupPath = @"C:\temp\Test for ArchiveGenerator\";
		public string Password = "NsW075eLyO45PzHs8bF4";
		public bool ReCreateConfigsIfXmlErrorOnParse = true;

		/// <summary> В этой папке ищем конфигурационные файлы </summary>
		public string SourcePath = @"C:\temp\Test for ArchiveGenerator\";

		public string WinRarPath = @"C:\Program Files\WinRAR\rar";

		/// <summary>Дополнительная командная строка для WinRar, помещается перед опциями директорий [ -ibck]</summary>
		public string RarCommandLine = " -ibck";//""

		#endregion Fields
	}
}
