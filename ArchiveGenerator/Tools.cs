﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace ArchiveGenerator
{
	/// <summary>
	///     Этот класс используется для передачи нужной кодировки в StringWriter
	/// </summary>
	internal sealed class XmlStringWriter : StringWriter
	{
		#region Fields (1) 

		private readonly Encoding encoding;

		#endregion Fields 

		#region Constructors (1) 

		public XmlStringWriter(Encoding encoding)
		{
			this.encoding = encoding;
		}

		#endregion Constructors 

		#region Properties (1) 

		public override Encoding Encoding
		{
			get { return encoding; }
		}

		#endregion Properties 
	}

	public static class Tools
	{
		#region Enums (1)

		/// <summary> Формат компрессии </summary>
		public enum CompressionFormat
		{
			//http://george.chiramattel.com/blog/2007/09/deflatestream-block-length-does-not-match.html
			/// <summary>
			///     Отличается от RFC1951 двумя байтами в начале последовательности.
			/// </summary>
			RFC1950,
			RFC1951
		}

		#endregion Enums

		/// <summary> Пытается сравнить 2 объекта через сериализацию </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <returns></returns>
		public static bool IsSame<T>(T expected, T actual)
		{
			string expectedSer = SerializeObjectDynamic(expected);
			string actualSer = SerializeObjectDynamic(actual);
			return expectedSer == actualSer;
		}

		#region Methods (21)

		// Public Methods (19) 

		public static T DeserializeObject<T>(string xml)
		{
			Contract.Requires(!String.IsNullOrEmpty(xml));
			var doc = new XmlDocument();

			//HACK-WEB: Защита от кривой сериализации
			if (xml[0] == 65279)
			{
				xml = xml.Substring(1, xml.Length - 1);
			}
			doc.LoadXml(xml);
			return DeserializeObject<T>(doc);
		}

		/// <summary>
		///     Reconstruct an object from an XML string (UTF-8)
		/// </summary>
		/// <returns></returns>
		public static T DeserializeObject<T>(XmlDocument doc)
		{
			Contract.Requires(doc != null);
			var xs = new XmlSerializer(typeof (T));
			if (doc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
			{
				var decl = (XmlDeclaration) doc.FirstChild;
				decl.Encoding = "UTF-8";
			}
			using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(doc.OuterXml)))
				return (T) xs.Deserialize(memoryStream);
		}

		public static T LoadObjectFromFile<T>(string filepath)
		{
			var doc = new XmlDocument();
			doc.Load(filepath);
			var dirSetting = DeserializeObject<T>(doc);
			return dirSetting;
		}

		public static void SaveConfigToFile<T>(T dirSettings, string filepath)
		{
			var doc = new XmlDocument();
			doc.LoadXml(SerializeObjectDynamic(dirSettings));
			doc.Save(filepath);
		}

		/// <summary>
		///     Является ли аргумент почтовым мылом
		/// </summary>
		/// <param name="email">проверяемый кандидат</param>
		public static bool IsEmail(string email)
		{
			Contract.Requires(!String.IsNullOrEmpty(email));
			//string PATTERN_STAR = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|ru|aero|pro|tv|[a-zA-Z]{2})$"; //Этот паттерн применяется в СТАР, а в веб используется более продвинутый (по согласованию с maverick)
			const string PATTERN_CYRILLIC =
				@"^[-а-яa-zА-ЯA-ZёЁ0-9][-.а-яa-zА-ЯA-ZёЁ0-9]*@[-.а-яa-zА-ЯA-ZёЁ0-9]+(\.[-.а-яa-zА-ЯA-ZёЁ0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|ru|aero|pro|tv|рф|[а-яa-zА-ЯA-ZёЁ]{2})$";
			return new Regex(PATTERN_CYRILLIC, RegexOptions.IgnorePatternWhitespace).IsMatch(email);
		}

		public static bool IsIpAddress(string testString)
		{
			Contract.Requires(!String.IsNullOrEmpty(testString));
			/*
						const string regexV4 = @"(2[0-4]\d|25[0-5]|[01]?\d\d?)(\.(2[0-4]\d|25[0-5]|[01]?\d\d?)){3}";
						const string regexV6 =
							@"(:(:|(:[\dA-Fa-f]{1,4}){1,7}))|([\dA-Fa-f]{1,4}:(:|(:[\dA-Fa-f]{1,4}){1,6}))|(([\dA-Fa-f]{1,4}:){2}(:|(:[\dA-Fa-f]{1,4}){1,5}))|(([\dA-Fa-f]{1,4}:){3}(:|(:[\dA-Fa-f]{1,4}){1,4}))|(([\dA-Fa-f]{1,4}:){4}(:|(:[\dA-Fa-f]{1,4}){1,3}))|(([\dA-Fa-f]{1,4}:){5}(:|(:[\dA-Fa-f]{1,4}){1,2}))|(([\dA-Fa-f]{1,4}:){6}(:|(:[\dA-Fa-f]{1,4})))|(([\dA-Fa-f]{1,4}:){7}(:|[\dA-Fa-f]{1,4}))|((:(:|(:[\dA-Fa-f]{1,4}){1,5}:))|([\dA-Fa-f]{1,4}:(:|(:[\dA-Fa-f]{1,4}){1,4}:))|(([\dA-Fa-f]{1,4}:){2}(:|(:[\dA-Fa-f]{1,4}){1,3}:))|(([\dA-Fa-f]{1,4}:){3}(:|(:[\dA-Fa-f]{1,4}){1,2}:))|(([\dA-Fa-f]{1,4}:){4}(:|:[\dA-Fa-f]{1,4}:))|(([\dA-Fa-f]{1,4}:){5}:)|(([\dA-Fa-f]{1,4}:){6}))([01]?\d\d?|2[0-4]\d|25[0-5])(\.([01]?\d\d?|2[0-4]\d|25[0-5])){3}";
						return Regex.IsMatch(testString, regexV4) || Regex.IsMatch(testString, regexV6);
			*/
			IPAddress ipa;

			return IPAddress.TryParse(testString, out ipa);
		}

		/// <summary> Строка содержит только шестнадцатеричные символы  </summary>
		/// <param name="testString"></param>
		/// <remarks>Pure указывает, что этот метод не влияет на его аргументы (для использования в контрактах)</remarks>
		[Pure]
		public static bool IsStringContainsOnlyHexChars(string testString)
		{
			return Regex.IsMatch(testString, @"\A\b[0-9a-fA-F]+\b\Z");
		}

		/// <summary>
		///     Serialize an object into an XML string (В кодировке UTF-8)
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		private static string SerializeObject<T>(T obj, Encoding encoding = null)
		{
			try
			{
				if (encoding == null)
				{
					encoding = Encoding.Unicode;
				}
				var ser = new XmlSerializer(typeof (T));
				var settings = new XmlWriterSettings {Indent = true, OmitXmlDeclaration = false, Encoding = Encoding.Unicode};
				var emptyNamepsaces = new XmlSerializerNamespaces(new[] {XmlQualifiedName.Empty});

				var sb = new StringBuilder();
				//using (var serializedObject = new StringWriterWithEncoding(sb, Encoding.UTF8))
				using (var stream = new XmlStringWriter(encoding))
				using (XmlWriter writer = XmlWriter.Create(stream, settings))
				{
					ser.Serialize(writer, obj, emptyNamepsaces);
					return stream.ToString();
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		///     Сериализация объекта в XML
		/// </summary>
		/// <param name="obj">Объект</param>
		/// <param name="encoding">Кодировка (если указать null, то будет использоваться кодировка UTF16)</param>
		/// <returns>Сериализованный объект</returns>
		public static string SerializeObjectDynamic(dynamic obj, Encoding encoding = null)
		{
			if (encoding == null)
			{
				encoding = Encoding.Unicode;
			}
			return SerializeObject(obj, encoding);
		}

		public static string SerializeObjectUtf8<T>(T obj)
		{
			var sb = new StringBuilder();
			using (var serializedObject = new StringWriterWithEncoding(sb, Encoding.UTF8))
			{
				var ser = new XmlSerializer(typeof (T));
				ser.Serialize(serializedObject, obj);
				return sb.ToString();
			}
		}

		/// <summary>
		///     Конвертирует String -> Decimal с учётом десятичной точки.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static decimal ToDecimalExt(this string value)
		{
			Decimal result;
			try
			{
				result = Decimal.Parse(value.Replace(".", ","));
			}
			catch
			{
				try
				{
					result = Decimal.Parse(value.Replace(",", "."));
				}
				catch
				{
					throw;
				}
			}
			return result;
		}

		/// <summary>
		///     Конвертирует String -> Decimal с учётом десятичной точки.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static Double ToDoubleExt(this string value)
		{
			Double result;
			try
			{
				result = Double.Parse(value.Replace(".", ","));
			}
			catch
			{
				try
				{
					result = Double.Parse(value.Replace(",", "."));
				}
				catch
				{
					throw;
				}
			}
			return result;
		}

		public static string ToStringRus(this DateTime dateTime)
		{
			return dateTime.ToString("dd.MM.yyyy HH:mm:ss");
		}

		public static string ToTittleCase(this string str)
		{
			return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(str);
		}

		/// <summary> Распаковывает строку из массива байт двумя методами (RFC1950, RFC1951) </summary>
		/// <param name="input">Массив байт</param>
		/// <param name="format">Тип декомпрессии</param>
		/// <returns>Строка в UTF-16</returns>
		public static string UnZipStr(byte[] input, CompressionFormat format)
		{
			using (var inputStream = new MemoryStream(input))
			{
				if (format == CompressionFormat.RFC1950)
				{
					inputStream.ReadByte();
					inputStream.ReadByte();
				}
				using (var gzip = new DeflateStream(inputStream, CompressionMode.Decompress))
				{
					using (var reader = new StreamReader(gzip, Encoding.Unicode))
					{
						return reader.ReadToEnd();
					}
				}
			}
		}

		/// <summary> Упаковывает строку в массив байт методом Вуадфеу (RFC1951) </summary>
		/// <param name="str">Строка в UTF-16</param>
		/// <returns></returns>
		public static byte[] ZipStr(String str)
		{
			using (var output = new MemoryStream())
			{
				using (var gzip = new DeflateStream(output, CompressionMode.Compress))
				{
					using (var writer = new StreamWriter(gzip, Encoding.Unicode))
					{
						writer.Write(str);
					}
				}

				return output.ToArray();
			}
		}

		// Private Methods (2) 

		/// <summary>
		///     Преобразует строку в массив байт
		/// </summary>
		/// <param name="str"></param>
		private static byte[] GetBytes(string str)
		{
			//http://stackoverflow.com/questions/472906/net-string-to-byte-array-c-sharp
			var bytes = new byte[str.Length*sizeof (char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		/// <summary>
		///     Преобразует массив байт в строку
		/// </summary>
		/// <param name="bytes"></param>
		private static string GetString(byte[] bytes)
		{
			//http://stackoverflow.com/questions/472906/net-string-to-byte-array-c-sharp
			var chars = new char[bytes.Length/sizeof (char)];
			Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		#endregion Methods

		#region Nested Classes (1)

		/// <summary>Класс для возможности писать строки в UTF-8</summary>
		public class StringWriterWithEncoding : StringWriter
		{
			#region Fields (1)

			private readonly Encoding encoding;

			#endregion Fields

			#region Constructors (1)

			public StringWriterWithEncoding(StringBuilder builder, Encoding encoding)
				: base(builder)
			{
				Contract.Requires(builder != null);
				this.encoding = encoding;
			}

			#endregion Constructors

			#region Properties (1)

			public override Encoding Encoding
			{
				get { return encoding; }
			}

			#endregion Properties
		}

		#endregion Nested Classes
	}
}